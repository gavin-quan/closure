package com.koudaiplus.closure

import com.google.gson.GsonBuilder
import org.junit.Test
import java.util.*

class ClosureTreeTest {

    @Test
    fun testBuildTree() {
        val list = listOf(
                Closure(id = 1, ancestor = "A", descendant = "A", distance = 0),
                Closure(id = 2, ancestor = "A", descendant = "C", distance = 1),
                Closure(id = 3, ancestor = "A", descendant = "D", distance = 1),
                Closure(id = 4, ancestor = "A", descendant = "E", distance = 2),
                Closure(id = 5, ancestor = "A", descendant = "F", distance = 2),
                Closure(id = 6, ancestor = "A", descendant = "G", distance = 2),
                Closure(id = 6, ancestor = "C", descendant = "A", distance = 2),
                Closure(id = 7, ancestor = "H", descendant = "H", distance = 0),
                Closure(id = 8, ancestor = "H", descendant = "I", distance = 1),
                Closure(id = 9, ancestor = "H", descendant = "J", distance = 1),
                Closure(id = 10, ancestor = "H", descendant = "J", distance = 1),
                Closure(id = 11, ancestor = "H", descendant = "J", distance = 1),
                Closure(id = 12, ancestor = "H", descendant = "K", distance = 1)
        )
        val hash = mapOf (
                Pair("A", "A hello"),
                Pair("B", "B hello"),
                Pair("C", "C hello"),
                Pair("D", "D hello"),
                Pair("E", "E hello"),
                Pair("F", "F hello"),
                Pair("G", "G hello"),
                Pair("H", "H hello"),
                Pair("I", "I hello"),
                Pair("J", "J hello"),
                Pair("K", "K hello")
        )
        val tree = ClosureTreeBuilder().getTree(closures = LinkedList(list), hashItem = hash)
        println(GsonBuilder().create().toJson(tree))
    }

}