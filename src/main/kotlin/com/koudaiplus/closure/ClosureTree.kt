/**
 * Copyright 2010-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.koudaiplus.closure

import java.util.*
import kotlin.collections.HashMap

data class ClosureTree<T> (
        val meta: Closure?,
        val data: T?,
        var children: LinkedList<ClosureTree<T>>?
)

class ClosureTreeBuilder (
        private val includeMeta: Boolean = true
) {

    fun getTree(closures: LinkedList<Closure>): List<ClosureTree<Any>> = getTree(closures = closures, hashItem = null)

    fun <T> getTree(closures: LinkedList<Closure>, hashItem: Map<String, T>?): List<ClosureTree<T>> {
        if(isEmptyCollection(closures)) return emptyList()
        val hashIndex = HashMap<String, ClosureTree<T>>(closures.size) //this key is descendant
        val hashGroup = HashMap<String, LinkedList<ClosureTree<T>>>(10) //this key is ancestor and do guess capacity
        val roots = LinkedList<String>() //this element is ancestor
        closures.forEach { closure ->
            val tree = ClosureTree(meta = if (includeMeta) closure else null, data = hashItem?.get(closure.descendant), children = null)
            if(closure.distance == 0) roots.add(closure.ancestor) //it is root node if distance is equal to 0, so be careful!!!
            if(!hashIndex.containsKey(closure.descendant)) hashIndex[closure.descendant] = tree //it is node keyword for every descendant
            if(closure.descendant != closure.ancestor) { //it is not group if descendant is equal to ancestor
                if(!hashGroup.containsKey(closure.ancestor)) hashGroup[closure.ancestor] = LinkedList() //it is group keyword for ancestor
                hashGroup[closure.ancestor]!!.add(tree)
            }
        }
        if(isEmptyCollection(roots)) return emptyList()
        hashGroup.entries.forEach { entry ->
            if(hashIndex.containsKey(entry.key)) hashIndex[entry.key]?.children = hashGroup[entry.key] //mount group by reference!!!
        }
        val tree = LinkedList<ClosureTree<T>>()
        roots.forEach { root ->
            tree.add(hashIndex[root]!!) //build tree structure
        }
        hashIndex.clear()
        hashGroup.clear()
        roots.clear()
        return tree
    }

    private fun isEmptyCollection(coll : Collection<*>?) = coll == null || coll.isEmpty()

}